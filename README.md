
## Getting Started

### Dependencies:
Tools needed to run this app: `node` and `yarn`


### Installing:
* `yarn install` to install dependencies

### Running the App:
After you have installed all dependencies, you may run the app.

- `yarn start`
- Go to `http://localhost:8080/`

### Running Tests:
* `yarn test`
