/**
 * description of module here
 * @module UserProvider
 */
import React, { useState, useEffect, useContext } from 'react';
import firebase from 'firebase';
export const Usercontext = React.createContext();

/**
 * It is function which are controlling the overall applications 
 * @param   {string}       USER TOKEN ID is a first Parameter 
 * @returns {HTML}         return the Login user details 
 */

export const UserProvider = (props) => {
  const [session, setSession] = useState({ User: null, loading: true });
  useEffect(() => {
    const unSubscribe = firebase.auth().onAuthStateChanged((user) => {
      setSession({ user, loading: false });
    });

    return () => unSubscribe();
  }, []);

  return (
    <Usercontext.Provider value={session}>
      {!session.loading && props.children}
    </Usercontext.Provider>
  );
};

export const UseSession = () => {
  const session = useContext(Usercontext);
  return session;
};
