/**
 * description of module here
 * @module ModuleName
 */
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';


/**
 *  Description of namespace here.
 *  @namespace
 */
firebase.initializeApp({
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABSE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSENGING_SENDER_ID,
});

// console.log(process.env.REACT_APP_API_KEY);
// // console.log(process.env.REACT_APP_API_KEY);
// console.log(firebase.app().options);

export const firestore = firebase.firestore();
export const storage = firebase.storage();
