/**
 * description of module here
 * @module Users
 */
import { firestore } from './config';
import { users, notification, SuccessMessage } from '../common/Utilities';


/**
 * It is function which are responsible for creating a user 
 * @param   {object}       USER  is a first Parameter 
 * @returns {string}         return  a message
 */
export const createUserDocument = (user) => {
  firestore.collection(users).doc(user.uid).set({
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    DateTime: Date.now(),
  })
    .then(() => {
      notification.success(`${SuccessMessage}`);
    })
    .catch((error) => {
      notification.error(`${error}`);
    });
};
