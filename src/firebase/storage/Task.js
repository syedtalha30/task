import { firestore } from '../config';
import { Task, notification,SuccessMessage } from '../../common/Utilities';
import { v4 as uuidv4 } from 'uuid';

export const createTaskDocument = ({RefProjectID,Description,Title,Status,DateTime, UserName,UserID},success,error) =>
{
    const id=uuidv4();
    firestore.collection(Task).doc(id).set({
        RefProjectID: RefProjectID,
        Title: Title,
        Description: Description,
        Status:Status,
        UserName:UserName,
        DateTime:DateTime,
        UserID:UserID,
        Uid:id,
    })
    .then(() =>{
                 success(id);
        notification.success(`${SuccessMessage}`);
    })
    .catch(function(err) {
        error(err);
        notification.error(`${err}`);
    });
};

export const updateTaskDocument = ({
            RefProjectID,
            Description,
            Title,
            Status,
            DateTime,
            UserName,
            UserID,
            Uid
},success,error) => {
    firestore.doc(`/${Task}/${Uid}`).update({
        Description,
        Title,
        Status,
        DateTime,
        UserName,
        UserID
})
    .then(function(data) {
        notification.success(`${SuccessMessage}`);
        success(data);
    })
    .catch(function(err) {
        notification.error(`${err}`);
        error(err);
    });
  };