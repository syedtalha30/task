/**
 * description of module here
 * @module ModuleName
 */
import firebase from 'firebase';
import { createUserDocument } from './users';
import { notification, SuccessMessage } from '../common/Utilities';


/**
 * Takes four string and returns confirmation message.
 * @param   {string} firstName   first string
 * @param   {string} lastName    second string
 * @param   {string} password    third  string
 * @param   {string} email    fourth  string
 * 
 * @returns {string} 
 */
export const signup =  ({
  firstName, lastName, password, email,
}, success, error) => {
  let user;
   firebase.auth().createUserWithEmailAndPassword(email, password)
    .then((data) => {
      user = data.user;
       data.user.updateProfile({ displayName: `${firstName} ${lastName}` })
        .then(async () => {
           createUserDocument(user);
          notification.success(SuccessMessage);
        })
        .catch((err) => {
          console.log(err);
          error(err);
          notification.error(`${err}`);
        });
    }).catch((err) => {
      console.log(err);
      error(err);
      notification.error(`${err}`);
    });

  // notification.error(`${error}`);

  return user;
};

export const logOut = () => firebase.auth().signOut();

/**
 * Takes two string and returns object.
 * @param   {string} email       first string
 * @param   {string} password    second string
 * 
 * @returns {string}       login user object 
 */
export const login = ( ({ email, password }, success, error) => {
   firebase.auth().signInWithEmailAndPassword(email, password)
    .then((data) => {
      success(data.user);
    }).catch((e) => {
      error(e);
    });
});
