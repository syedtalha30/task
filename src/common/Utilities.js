
/**
 * description of module here
 * @module Utilities
 */
import { toast } from 'react-toastify';

/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const notification=toast;
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const Task ="task";
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const HistoryTask ="history";
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const Project ="projects";
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const users ="users";
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const SuccessMessage ="Successfully Saved !";
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const ErrorMessage ="Error Occour !";
/**
 * The environment we're running in.
 * @constant
 *
 * @type {string}
 */
export const DateType="en-US";


export const Dateoptions={
    year: "numeric",
    month: "long",
    day: "2-digit",
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
   };
