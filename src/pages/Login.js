import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { login } from '../firebase/auth';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';

function Login(props) {
  const { register, handleSubmit,errors, reset } = useForm();
  const [isLoading, setLoading] = useState(false);

  const routeOnLogin = (user) => {
    props.history.push(`/DashBoard/${user.uid}`);
  };

  const onSubmit =  (data) => {
    setLoading(true);
   login(data,
        (result)=>
        {
          routeOnLogin(result);
        },(err)=>
        {
         console.log(err);
         toast.error(err.message);
        });
      reset();
      setLoading(false);
  };

  const formClassName = `ui form ${isLoading ? 'loading' : ''}`;

  return (
      <>
          <form className={formClassName} onSubmit={handleSubmit(onSubmit)}>
              <label>
                Email </label>
                <input
                  type="email"
                  name="email"
                  placeholder="Email"
                  ref={register({ required: true })}
                />
            {errors.email && <p>input is required</p>}
              <label>
                Password
                </label>
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  ref={register({ required: true})}
                />
                {errors.password && <p>input is required</p>}
            <div >
              <input type="submit" value="Login"/>
            </div>
          </form>
    <ToastContainer 
        closeOnClick={false}
        draggable={false}/>
 </>
 );
}

export default Login;
