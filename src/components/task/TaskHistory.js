/**
 * description of module here
 * @module TaskHistory
 */
import React, { useState, useEffect,useContext } from 'react';
import { HistoryTask,notification } from '../../common/Utilities';
import {GetData } from '../../services/Helper';
import { TaskContext } from "../../context/ApplicationContext";
import { TableTaskContext } from "../../context/ApplicationContext";
import PopUp from '../template/PopUp';
import { useParams} from "react-router-dom";

/**
 * It is react component base function which are returning the Html view for Listing the each individual Task History.
 * @param   {string}       USER TOKEN ID is a first Parameter 
 * @param   {string}       TaskID is a second Parameter 
 * @param   {object}       object is a third Parameter  passing through Context API
 * @returns {HTML}         return the View 
 */
function TaskHistory() 
{
  const params = useParams();
    const [context, setContext] = useContext(TaskContext);
    const [Tablecontext, setTableContext] = useState();
    const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const fetchData = () => {
       GetData(params.TID,"ParentTaskID",HistoryTask,(data)=>
      {
        setTableContext(data);
      },
      (error)=>{
       notification.error(`${error}`);
      });
    };
    fetchData();
    setLoading(false);
    return () => fetchData();
  },[context])

  const formClassName = `ui form ${isLoading ? 'loading' : ''}`;
    return (
        <>
            <div className={formClassName}>
            <TableTaskContext.Provider value={[Tablecontext,setTableContext,true]}>
              <PopUp></PopUp>
            </TableTaskContext.Provider>
    </div>
        </>
    )
}

export default TaskHistory;
