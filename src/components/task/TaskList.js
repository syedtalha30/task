/**
 * description of module here
 * @module TaskList
 */

import React, {useState,useEffect,useContext} from 'react'
import {Task,ErrorMessage, notification} from '../../common/Utilities';
import { GetData } from '../../services/Helper';
import { TaskContext } from "../../context/ApplicationContext";
import { TableTaskContext } from "../../context/ApplicationContext";
import PopUp from '../template/PopUp';
import {
  useParams
} from "react-router-dom";


/**
 * It is react component base function which are returning the Html view for Listing the Task against each project.
 * @param   {string}       USER TOKEN ID is a first Parameter 
 * @param   {string}       ProjectId is a second Parameter 
 * @param   {object}       object is a third Parameter  passing through Context API
 * @returns {HTML}         return the View 
 */
function TaskList() 
{
  const params = useParams();
  const [context] = useContext(TaskContext);
  const [Tablecontext, setTableContext] = useState([]);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    GetData(params.PID,"RefProjectID",Task,
      (data)=>
      {
        setTableContext(data);
        setLoading(false);
      },(err)=>{
        notification.error(ErrorMessage);
        setLoading(false);
      });
  }, [context])

  const formClassName = `ui form ${isLoading ? 'loading' : ''}`;
  return (
    <>
    <TableTaskContext.Provider value={[Tablecontext]}>
     <div className={formClassName}>

      <PopUp></PopUp>    
  </div>
  </TableTaskContext.Provider>
    </>
  )
}

export default TaskList;
