/**
 * description of module here
 * @module ProjectList
 */
import React, {useState,useContext,useEffect} from 'react'
import { Link } from 'react-router-dom';
import {UseSession} from '../../firebase/UserProvider';
import {Dateoptions,DateType,Project,notification,ErrorMessage} from '../../common/Utilities'
import { GetInformation } from '../../services/Helper';

import { ProjectContext } from "../../context/ApplicationContext";

/**
 * It is react component base function which are returning the Html view for Listing the Projects.
 * @param   {string}       USER TOKEN ID is a first Parameter 
 * @returns {HTML}         return the View 
 */
function ProjectList() 
{
  const [context,setContext] = useContext(ProjectContext);
  const [getProjects, setProjects] = useState([]); 
  const {user}= UseSession();
  const [isLoading, setLoading] = useState(false);
 
  const GetProjects = (TableName) =>
     {
       setLoading(true);
        GetInformation(TableName,
       (result)=>
       {
         setProjects(result);
       },(err)=>
       {
         notification.error(ErrorMessage);
       });
       setLoading(false);
     }
   
  useEffect(() => 
  {
    GetProjects(Project);
  }, [context])

  const formClassName = `ui form ${isLoading ? 'loading' : ''}`;
    return (
      <>
       <div className={formClassName}>
       <div className="table-responsive text-nowrap">
            <table className="table table-bordered">
      <thead className="table table-striped table-dark">
          <tr>
            <th>Porject</th>
            <th>Date & Time</th>
            <th>User</th>
          </tr>
        </thead>
        <tbody>
          {getProjects.map((project) => (
          
            <tr key={project.Uid}>
              <td><Link to={`/ProjectTask/${user.uid}/${project.Uid}`}>{project.PorjectName}</Link></td>
              <td>
              {new Intl.DateTimeFormat(DateType, Dateoptions).format(project.DateTime)}
              </td>
              <td>{project.userName}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
      </>
    )
}

export default ProjectList;