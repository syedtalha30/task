/**
 * description of module here
 * @module NewTask
 */
import React, {useState,useContext} from 'react'
import { Modal } from 'react-bootstrap';
import { useForm,Controller } from 'react-hook-form';
import {createTaskDocument} from '../../firebase/storage/Task';
import {UseSession} from '../../firebase/UserProvider';
import {Status } from '../../services/Helper';
import { TaskContext } from "../../context/ApplicationContext";
import { notification} from '../../common/Utilities';
import { useParams } from "react-router-dom";
import { createTaskHistoryDocument } from '../../firebase/storage/History';


/**
 * It is react component base function which are returning the Html view for Creating a new Task.
 * @param   {string}       USER TOKEN ID is a first Parameter 
 * @param   {string}       Project  ID  is a first Parameter 
 * @returns {HTML}         return the View 
 */
function NewTask() 
 {
    const [context, setContext] = useContext(TaskContext);
    const params = useParams();
    const [show, setShow] = useState(false); 
    const { register, handleSubmit,errors,control } = useForm();
    const [isLoading, setLoading] = useState(false);
    const {user}= UseSession();
    const handleClose = () => setShow(false); 
    const handleShow = () => setShow(true); 

    const createTaskHistory = (data,ID) =>
    {
      createTaskHistoryDocument(
            {
              RefProjectID:data.RefProjectID,
              Description:data.Description,
              Title:data.Title,
              Status:data.Status,
              DateTime:data.DateTime,
              UserName:data.UserName,
              UserID:data.UserID,
              ParentTaskID:ID
            },()=>
            {
              setContext(data);
              setLoading(false);
              handleClose();
            },(err)=>{
              notification.error(`${err}`);
              setLoading(false);
              handleClose();
            });
    }
    const createTask = (PID,data) =>
    {
        setLoading(true);
        let taskDetails={
          RefProjectID:PID,
          Description:data.Description,
          Title:data.Title,
          Status:data.Status,
          DateTime:  Date.now(),
          UserName: user.displayName,
          UserID: user.uid
        };
      createTaskDocument(taskDetails
        ,(result)=>
        {
          createTaskHistory(taskDetails,result);
        },(err)=>{
          notification.error(`${err}`);
          setLoading(false);
          handleClose();
        });
    }
      const onSubmit = (data) => 
      {
        createTask(params.PID,data);
      };

      const formClassName = `ui form ${isLoading ? 'loading' : ''}`;
    return (
        <> 
        <br></br>
        <div className="row">
        <div className="col-sm-8">
          <h3 className="AddTask-header">Task List</h3>
        </div>
        <div className="col-sm-4">
          <button type="button" className="btn btn-success AddNew-button float-right" onClick = {handleShow}>Add</button>
        </div>
        <Modal show={show} onHide={handleClose} keyboard={false} >
          <Modal.Header>
            <Modal.Title>Add new Task</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form className={formClassName} onSubmit={handleSubmit(onSubmit)}>
                    <input type="text" ref={register({ required: true })}  id="Title" name="Title" placeholder="Enter Task"></input>
                <br></br>
                {errors.Title && <p>input is required</p>}
                    <input type="text" ref={register({ required: true })} id="Description" name="Description" placeholder="Enter Description"></input>
                    <br></br>
                    {errors.Description && <p>input is required</p>}
                
                <select name="Status" className="DropDownSelect" ref={register}>
                        {Status.map((option,index) => (
                      <option key={index} value={option.value}>{option.label}</option>
                    ))}
                </select>
                <input  type="submit" value="Submit" />
            </form>
          </Modal.Body>
        </Modal>
   </div>
 </> 
    );
}

export default NewTask;
