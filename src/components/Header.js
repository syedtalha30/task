import React from 'react';
 import { logOut } from '../firebase/auth';
import { useHistory } from 'react-router-dom';
import { UseSession } from '../firebase/UserProvider';
import {Navbar,Nav,
   NavbarBrand, NavItem, NavLink} from 'react-bootstrap';

function Header() {
  const history = useHistory();
   const { user } = UseSession();

  const logoutUser = () => {
     logOut();
    history.push('/login');
  };

  return (
    <>
      {!!user ? (
<Navbar className="navbar navbar-expand-lg navbar-dark bg-primary">
<NavbarBrand href={`/DashBoard/${user.uid}`}>
DashBoard
</NavbarBrand>
<Nav className="ml-auto" navbar>
  <NavItem className="d-flex align-items-center">
    <span className="font-weight-bold HeaderLoginUser">{user.displayName}</span>
  </NavItem>
  <NavItem className="d-flex align-items-center">
  <button className="btn btn-light float-right" onClick={logoutUser} type="button">LogOut</button>
  </NavItem>
</Nav>
</Navbar>
      ): (
        <Navbar className="navbar navbar-expand-lg navbar-dark bg-primary">
        <Navbar.Brand>Task Management</Navbar.Brand>
        <Nav className="ml-auto" navbar>
  <NavItem className="d-flex align-items-center">
  <NavbarBrand href='/Login'>Login</NavbarBrand>
  </NavItem>
  <NavItem className="d-flex align-items-center">
  <NavbarBrand href='/SignUp'>SignUp</NavbarBrand>
  </NavItem>
</Nav>
           </Navbar>
      )}
    </>
  );
}

export default Header;
