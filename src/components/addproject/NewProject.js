/**
 * description of module here
 * @module NewProject
 */
import React, {useState,useContext} from 'react'
import { Modal } from 'react-bootstrap';
import createProjectDocument from '../../firebase/storage/Project';
import {UseSession} from '../../firebase/UserProvider';
import { useForm } from 'react-hook-form';
import { ProjectContext } from "../../context/ApplicationContext";

/**
 * It is react component base function which are returning the Html view for Creating a new Project.
 * @param   {string}       USER TOKEN ID 
 * @returns {HTML}         return the View 
 */
function NewProject() 
 {
    const [context,setContext] = useContext(ProjectContext);
    const { register, handleSubmit,errors } = useForm();
    const [show, setShow] = useState(false); 
    const {user}= UseSession();
    const [isLoading, setLoading] = useState(false);

    const handleClose = () => setShow(false); 
    const handleShow = () => setShow(true);

      const handleAdd= (data)=>
      {
        setLoading(true);
         createProjectDocument(
          {PorjectName: data.ProjectName,
            DateTime:  Date.now(), 
            userName: user.displayName,
            UserID: user.uid
          });
      setLoading(false);
      setContext(data);
        handleClose();
      }
      const formClassName =`ui form ${isLoading ? 'loading' : ''}`;
      return (
        <> 
        <br></br>
             <div className="row">
    <div className="col-sm">
    <h3 className="AddNew-header"> Projects</h3>
    </div>
    <div className="col-sm">
    <button type="button" className="btn btn-success AddNew-button float-right" onClick = {handleShow}>Add</button>
</div>
             <Modal show={show} onHide={handleClose} keyboard={false} >
               <Modal.Header>
                 <Modal.Title>Add New Project</Modal.Title>
               </Modal.Header>
               <Modal.Body>
                 <form className={formClassName} onSubmit={handleSubmit(handleAdd)}>
                    
                         <input type="text" ref={register({ required: true })} id="inputTask" name="ProjectName" placeholder="Enter Project Name"></input>
                         {errors.ProjectName && <p>input is required</p>}
                         <input  type="submit" value="Submit" />
                 </form>
               </Modal.Body>
             </Modal>
        </div>
      </> 
      );
}

export default NewProject;
