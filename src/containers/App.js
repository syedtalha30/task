
/**
 * description of module here
 * @module App
 */

import '../firebase/config';
import {BrowserRouter as Router , Switch, Route, Redirect} from 'react-router-dom';
import Signup from '../pages/Signup';
import Header from '../components/Header';
import {UserProvider} from '../firebase/UserProvider';
import Login from '../pages/Login';
import {ProfileRedirect} from '../router/ProfileRedirect';
import {PrivateRoute} from '../router/PrivateRoute';
import DashBoard from '../pages/DashBoard';
import ProjectTask from '../pages/ProjectTask';
import TaskDetails from '../pages/TaskDetails';
import { ToastContainer } from 'react-toastify';
import React from 'react';

/**
 * It is react component base function which are returning the Html view of header along with the login user
 * @param   {string}       USER TOKEN ID is a first Parameter 
 * @param   {object}       object is a third Parameter  passing through Context API
 * @returns {HTML}         return the View 
 */
function App() {
  return (
    <UserProvider>
       <Router>
      <Header></Header>
      <ToastContainer 
        closeOnClick={false}
        draggable={false}/>
      <div className="app">
          <div className="ui grid container">
      <Switch>
        <ProfileRedirect exact path="/signup" component={Signup}></ProfileRedirect>
        <PrivateRoute path='/ProjectTask/:id?/:PID?' component={ProjectTask}></PrivateRoute>
        <PrivateRoute path='/TaskDetails/:id?/:TID?' component={TaskDetails}></PrivateRoute>
        <PrivateRoute path='/DashBoard/:id' component={DashBoard}></PrivateRoute>
        <ProfileRedirect path='/Login' component={Login}></ProfileRedirect>
        <Route exact path='/'>
          <Redirect to="/Login"></Redirect>
        </Route>
      </Switch>
    </div>
    </div>
    </Router>
    </UserProvider>
  );
}

export default App;
