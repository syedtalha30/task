/**
 * description of module here
 * @module ModuleName
 */
import React from 'react';

const TaskContext = React.createContext();

export default TaskContext;