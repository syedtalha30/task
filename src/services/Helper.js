/**
 * description of module here
 * @module Helper
 */
import { firestore } from '../firebase/config';
import {ErrorMessage} from '../common/Utilities';


/**
 *  Description of namespace here.
 *  @namespace
 */
export const Status=[
    {
        value:"TODO",
        label:"TO DO"
    },
    {
        value:"Blocked",
        label:"Blocked"
    },
    {
        value:"InProgress",
        label:"In Progress"
    },
    {
        value:"InQA",
        label:"In QA"
    },{
        value:"Done",
        label:"Done"
    },
    {
        value:"Deployed",
        label:"Deployed"
    }];

/**
 * Takes a string  and returns the Status Value.
 * @param   {string}
 *
 * @returns {string} return the Satring value 
 */
    export const GetStatus = (value) =>
{
    const text=Status.filter(element => element.value===value)[0].label;
 return text;
}

/**
 * Takes a string  and returns the Status Value.
 * @param   {string}          TID is the First Parameter
 * @param   {string}          columnName is the Second columnName
 * @param   {string}          TableName is the Third Parameter
 * @param   {function}        success is the Fourth Parameter
 * @param   {function}        error is the fifth Parameter
 * 
 * @returns {fucntion}         return the function 
 */
export const GetData = (TID,columnName,TableName,success,error)=>
{
  let dataToFill=[];
  
   firestore.collection(TableName).where(columnName, '==', TID).orderBy('DateTime', 'desc')
  .get()
  .then((querySnapshot) =>{
    
          if(querySnapshot.size > 0)
          {
            querySnapshot.forEach(function(doc) 
            {
              dataToFill.push(doc.data());
            });
            
          }
          success(dataToFill);
          // else{
          //   error("No Record Found !");
          // }
  })
  .catch(function(er) {
    console.log(er);
    error(ErrorMessage);
  });
}



/**
 * Takes a string  and returns the Callback function.
 * @param   {string}          TableName is the First Parameter
 * @param   {string}          success is the Second Parameter
 * @param   {string}          error is the Third Parameter
 * 
 * @returns {fucntion}         return the function 
 */
export const GetInformation =  (TableName,success,error) =>
{
  let dataToFill=[];
   firestore.collection(TableName).orderBy('DateTime', 'desc')
  .get()
  .then((querySnapshot) =>{
          if(querySnapshot.size > 0)
          {
            querySnapshot.forEach(function(doc) 
            {
              dataToFill.push(doc.data());
            });
            
          }
          success(dataToFill);
  })
  .catch(function(er) {
    console.log(er);
    error(ErrorMessage);
  });
};


/**
 * Takes a string  and returns the Callback function.
 * @param   {string}          TableName is the First Parameter
 * @param   {string}          success is the Second Parameter
 * @param   {string}          error is the Third Parameter
 * 
 * @returns {fucntion}         return the function 
 */
export const GetTaskDetails = (TableName,columnName,Param,success,error) =>{
  
   firestore.collection(TableName).where(columnName, '==', Param)
  .get().then((querySnapshot)=>
   {
    if(querySnapshot.size > 0)
    {
      querySnapshot.forEach(function(doc) 
            {
              success(doc.data());
            });
     
    } else {
      error("No Record Found !");
    }
}).catch(function(er) {
  console.log(er);
  error(ErrorMessage);
});
}